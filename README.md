# Guide to setting-up WSL Ubuntu

This repo is a working progress. It sets up Ubuntu on WSL (v1). Setup zsh as shell and configures all the zsh goodness

### Running locally


```sh
git clone https://gitlab.com/divemarkus/wsl-ubuntu.git
cd wsl-ubuntu
```

### Instructions

Setup Ubuntu with credentials and perform updates

`sudo apt-get update && apt-get upgrade`

Install Ansible and python interpreter (in order to run its modules):

`sudo apt-add-repository ppa:ansible/ansible`

`sudo apt-get update`

`sudo apt-get install ansible -y`

`sudo apt-get install python -y`

`sudo apt-get install git -y`

Once you've completed above steps, you will need to perform the ansible-pull

* change your default shell to zsh
* exit out of WSL
* open WSL and answer the prompts
* you may need to exit and open WSL again
* auto-completion should now work
* the look and feel of zsh should be noticable
* icons and arrows are broken
* more to add here...
